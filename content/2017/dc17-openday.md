Title: DebConf17 Open Day
Slug: dc17-openday
Date: 2017-08-05 16:00
Author: Laura Arjona Reina
Tags: debconf17, debconf, openday
Status: published

Today, the day preceeding the official start of the annual Debian
Conference, is the **Open Day** at [DebConf17](https://debconf17.debconf.org/),
at [Collège Maisonneuve](https://wiki.debconf.org/wiki/DebConf17/Venue)
in Montreal (Canada).

This day is open to the public with events of interest to a wide audience.

The schedule of today's events include, among others:

* A Newbie's Newbie Guide to Debian
* Ask Anything About Debian
* Debian Packaging 101
* Debian InstallFest
* Presentations or workshops related to free software projects and local organizations.

Everyone is welcome to attend! It is a great possibility for interested users
to meet our community and for Debian to widen our community.

See the full schedule for today's events at
[https://debconf17.debconf.org/schedule/open-day/](https://debconf17.debconf.org/schedule/open-day/).

If you want to engage remotely, you can watch the
[**video streaming**](https://debconf17.debconf.org/schedule/venue/3/)
of the Open Day events happening in the "Rex" room,
or join the conversation in the channels
 [**#debconf17-rex**](https://webchat.oftc.net/?channels=#debconf17-rex),
 [**#debconf17-potato**](https://webchat.oftc.net/?channels=#debconf17-potato) and
 [**#debconf17-woody**](https://webchat.oftc.net/?channels=#debconf17-woody)
in the OFTC IRC network.

DebConf is committed to a safe and welcome environment for all participants.
See the [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml)
and the [Debian Code of Conduct](https://www.debian.org/code_of_conduct) for more details on this.

Debian thanks the commitment of numerous [sponsors](https://debconf17.debconf.org/sponsors/)
to support DebConf17, particularly our Platinum Sponsors
[**Savoir-Faire Linux**](https://www.savoirfairelinux.com/),
[**Hewlett Packard Enterprise**](http://www.hpe.com/engage/opensource),
and [**Google**](https://google.com/).

![DebConf17 logo](|static|/images/800px-Dc17logo.png)
