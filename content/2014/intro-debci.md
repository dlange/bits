Title: Introducing the Debian Continuous Integration project
Date: 2014-06-12 00:01
Tags: qa, announce
Slug: intro-debci
Author: Ana Guerrero Lopez
Status: published

Debian is a big system. At the time of writing, the unstable distribution has
more than 20,000 source packages, building more then 40,000 binary packages on
the amd64 architecture. The number of inter-dependencies between binary
packages is mind-boggling: the entire dependency graph for the amd64
architecture contains a little more than 375,000 edges. If you want to expand
the phrase "package A depends on package B", there are more than 375,000 pairs
of packages A and B that can be used.

Every one of these dependencies is a potential source of problems. A library
changes the semantics of a function call, and then programs using that library
that assumed the previous semantics can start to malfunction. A new version of
your favorite programming language comes out, and a program written in it no
longer works. The number of ways in which things can go wrong goes on and on.

With an ecosystem as big as Debian, it is just impossible to stop these
problems from happening. What we can do is trying to detect when they happen,
and fix them as soon as possible.

[The Debian Continuous Integration](http://ci.debian.net/) project was created
to address exactly this problem. It will continuously run test suites for
source packages when any of their dependencies is updated, as well as when a
new version of the package itself is uploaded to the unstable distribution. If
any problems that can be detected by running an automated test suite arise,
package maintainers can be notified in a matter of hours.

Antonio Terceiro has posted on his blog an [introduction to the
project](http://softwarelivre.org/terceiro/blog/an-introduction-to-the-debian-continuous-integration-project)
with a more detailed description of the project, its evolution since January
2014 when it was first introduced, an explanation of how the system works, and
how maintainers can enable test suites for their packages. You might also want
to check [the documentation](http://ci.debian.net/doc/) directly.
