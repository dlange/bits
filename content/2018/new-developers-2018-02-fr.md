Title: Nouveaux développeurs et mainteneurs de Debian (janvier et février 2018)
Slug: new-developers-2018-02
Date: 2018-03-04 08:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

  * Alexandre Mestiashvili (mestia)
  * Tomasz Rybak (serpent)
  * Louis-Philippe Véronneau (pollo)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

  * Teus Benschop
  * Kyle John Robbertze
  * Maarten van Gompel
  * Dennis van Dok
  * Innocent De Marchi
  * David Rabel


Félicitations !
