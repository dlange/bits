Title: Nous desenvolupadors i mantenidors de Debian (juliol i agost del 2019)
Slug: new-developers-2019-08
Date: 2019-09-17 17:30
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developersen els darrers dos mesos:

  * Keng-Yu Lin (kengyu)
  * Judit Foglszinger (urbec)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Hans van Kranenburg
  * Scarlett Moore

Enhorabona a tots!

