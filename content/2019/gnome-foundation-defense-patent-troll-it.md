Title: The Debian Project stands with the GNOME Foundation in defense against patent trolls
Slug: gnome-foundation-defense-patent-troll
Date: 2019-10-23 10:00
Author: Ana Guerrero López
Tags: debian, gnome, patent trolls, fundraising
Status: published
Lang: it
Translator: Sebastiano Pistore

Nel 2012 il Progetto Debian ha stabilito ufficialmente la propria [Posizione sui brevetti del software],
dichiarando che i brevetti sono una minaccia per il Software Libero.

La Fondazione GNOME ha recentemente dichiarato di essere stata coinvolta in un procedimento giudiziario
riguardante Shotwell, un programma Libero di gestione delle immagini,
che è stato accusato di violazione di brevetto.

Il Progetto Debian rimarrà vicino alla Fondazione GNOME nel corso di questo vile attacco
perchè diventi chiaro a tutti che il Software Libero è sviluppato, e anche difeso, da Community che collaborano tra loro
se vengono attaccate abusando delle leggi esistenti.

Dovreste leggere il [post originale di GNOME su questa vicenda]
ed eventualmente pensare se potete contribuire con una donazione allo [GNOME Patent Troll Defense Fund].

[Posizione sui brevetti del software]: https://www.debian.org/legal/patent
[post originale di GNOME su questa vicenda]:  https://www.gnome.org/news/2019/10/gnome-files-defense-against-patent-troll/
[GNOME Patent Troll Defense Fund]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
