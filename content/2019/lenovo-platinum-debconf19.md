Title: Lenovo Platinum Sponsor of DebConf19
Slug: lenovo-platinum-debconf19
Date: 2019-05-20 10:00
Author: Laura Arjona Reina
Artist: Lenovo
Tags: debconf19, debconf, sponsors, lenovo
Status: published

[![lenovologo](|static|/images/lenovo.svg)](https://www.lenovo.com/)

We are very pleased to announce that [**Lenovo**](https://www.lenovo.com/)
has committed to supporting [DebConf19](https://debconf19.debconf.org) as a **Platinum sponsor**.

*"Lenovo is proud to sponsor the 20th Annual Debian Conference."*
said Egbert Gracias, Senior Software Development Manager at Lenovo. *"We’re excited to see, 
up close, the great work being done in the community and to meet the developers
and volunteers that keep the Debian Project moving forward!”*

Lenovo is a global technology leader manufacturing a wide portfolio of connected
products, including smartphones, tablets, PCs and workstations as well as AR/VR 
devices, smart home/office solutions and data center solutions.

With this commitment as Platinum Sponsor,
Lenovo is contributing to make possible our annual conference,
and directly supporting the progress of Debian and Free Software,
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Lenovo, for your support of DebConf19!

## Become a sponsor too!

DebConf19 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf19 website at [https://debconf19.debconf.org](https://debconf19.debconf.org).
